json.array!(@articles) do |article|
  json.extract! article, :id, :title, :preview, :text, :author
  json.url article_url(article, format: :json)
end
