class PagesController < ApplicationController

  def main
    @articles = Article.limit(3)
  end

  def md_preview
    @text = params[:data]
    render "md_preview", layout: false
  end

  def manifest
  end

  def donate
  end

  def new_author
  end
end
