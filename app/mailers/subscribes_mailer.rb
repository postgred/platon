class SubscribesMailer < ApplicationMailer
  default from: 'from@example.com'

  def new_article(email)
    mail(to: email, subject: 'New Article')
  end
end
