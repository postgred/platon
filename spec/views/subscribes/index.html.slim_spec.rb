require 'rails_helper'

RSpec.describe "subscribes/index", type: :view do
  before(:each) do
    assign(:subscribes, [
      Subscribe.create!(
        :email => "Email"
      ),
      Subscribe.create!(
        :email => "Email"
      )
    ])
  end

  it "renders a list of subscribes" do
    render
    assert_select "tr>td", :text => "Email".to_s, :count => 2
  end
end
