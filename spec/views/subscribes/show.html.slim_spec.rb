require 'rails_helper'

RSpec.describe "subscribes/show", type: :view do
  before(:each) do
    @subscribe = assign(:subscribe, Subscribe.create!(
      :email => "Email"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Email/)
  end
end
