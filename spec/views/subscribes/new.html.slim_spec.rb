require 'rails_helper'

RSpec.describe "subscribes/new", type: :view do
  before(:each) do
    assign(:subscribe, Subscribe.new(
      :email => "MyString"
    ))
  end

  it "renders new subscribe form" do
    render

    assert_select "form[action=?][method=?]", subscribes_path, "post" do

      assert_select "input#subscribe_email[name=?]", "subscribe[email]"
    end
  end
end
