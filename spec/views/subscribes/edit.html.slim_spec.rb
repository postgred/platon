require 'rails_helper'

RSpec.describe "subscribes/edit", type: :view do
  before(:each) do
    @subscribe = assign(:subscribe, Subscribe.create!(
      :email => "MyString"
    ))
  end

  it "renders the edit subscribe form" do
    render

    assert_select "form[action=?][method=?]", subscribe_path(@subscribe), "post" do

      assert_select "input#subscribe_email[name=?]", "subscribe[email]"
    end
  end
end
