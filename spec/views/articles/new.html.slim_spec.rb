require 'rails_helper'

RSpec.describe "articles/new", type: :view do
  before(:each) do
    assign(:article, Article.new(
      :title => "MyString",
      :preview => "MyText",
      :text => "",
      :author => "MyString"
    ))
  end

  it "renders new article form" do
    render

    assert_select "form[action=?][method=?]", articles_path, "post" do

      assert_select "input#article_title[name=?]", "article[title]"

      assert_select "textarea#article_preview[name=?]", "article[preview]"

      assert_select "input#article_text[name=?]", "article[text]"

      assert_select "input#article_author[name=?]", "article[author]"
    end
  end
end
