working_directory "/home/platon/platon"

# Расположение PID Файла
# Я, обычно, распологаю его в папка_приложения/tmp/pids/unicorn.pid
pid "/home/platon/platon/tmp/pids/unicorn.pid"

# Папка с логами
# Я держу их в папка_приложения/log/unicorn.log
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/home/platon/platon/log/unicorn.log"
stdout_path "/home/platon/platon/log/unicorn.log"

# Unicorn socket
# В моем случае это папка_приложения/tmp/sockets/
listen "/home/platon/platon/tmp/sockets/unicorn.[app name].sock"
listen "/home/platon/platon/tmp/sockets/unicorn.myapp.sock"

# Кол-во процессов
# worker_processes 4
worker_processes 2

# Тайм-аут
timeout 30
